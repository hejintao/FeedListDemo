//
//  PRTimelineMaster.m
//  ProCloser
//
//  Created by 何锦涛 on 2018/4/4.
//  Copyright © 2018年 HZMC. All rights reserved.
//

#import "PRTimelineMaster.h"

@implementation PRTimelineMaster

+ (NSString *)dateForFormatOneWithTimeStamp:(NSString *)timeStamp {
    // 都是从1970开始
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStamp integerValue]];
    // 用于NSdate对象的格式化
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // 设置格式 zzz表示时区
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    // 设置地区
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en"];
    // NSDate转NSString
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

@end
