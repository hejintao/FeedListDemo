//
//  PRTimelineMaster.h
//  ProCloser
//
//  Created by 何锦涛 on 2018/4/4.
//  Copyright © 2018年 HZMC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRTimelineMaster : NSObject

/** 时间  */
+ (NSString *)dateForFormatOneWithTimeStamp:(NSString *)timeStamp;

@end
