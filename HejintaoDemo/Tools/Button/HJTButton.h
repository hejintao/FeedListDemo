/*
 HJTButton
 */
#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface HJTButton : UIButton

typedef NS_ENUM(NSUInteger, HJTButtonImagePosition)
{
    HJTButtonImagePositionLeft   = 0,
    HJTButtonImagePositionRight  = 1,
    HJTButtonImagePositionTop    = 2,
    HJTButtonImagePositionBottom = 3,
    HJTButtonImagePositionCenter = 4
};

typedef NS_ENUM(NSUInteger, HJTButtonTitlePosition)
{
    HJTButtonTitlePositionRight  = 0,
    HJTButtonTitlePositionLeft   = 1,
    HJTButtonTitlePositionBottom = 2,
    HJTButtonTitlePositionTop    = 3,
    HJTButtonTitlePositionCenter = 4
};

@property (assign, nonatomic) IBInspectable HJTButtonImagePosition imagePosition;
/** Default is 6.f */
@property (assign, nonatomic) IBInspectable CGFloat imageSpacingFromTitle;
@property (assign, nonatomic) IBInspectable CGPoint imageOffset;

@property (assign, nonatomic) IBInspectable HJTButtonTitlePosition titlePosition;
/** Default is 6.f */
@property (assign, nonatomic) IBInspectable CGFloat titleSpacingFromImage;
@property (assign, nonatomic) IBInspectable CGPoint titleOffset;

@property (strong, nonatomic, readonly) IBInspectable UIImage *maskImage;

@property (assign, nonatomic, getter=isTitleLabelWidthUnlimited)    IBInspectable BOOL titleLabelWidthUnlimited;
@property (assign, nonatomic, getter=isAdjustsAlphaWhenHighlighted) IBInspectable BOOL adjustsAlphaWhenHighlighted;
@property (assign, nonatomic, getter=isAnimatedStateChanging)       IBInspectable BOOL animatedStateChanging;

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state;

- (void)setMaskAlphaImage:(UIImage *)maskImage;
- (void)setMaskBlackAndWhiteImage:(UIImage *)maskImage;

@end

