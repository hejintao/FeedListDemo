//
//  AppDelegate.h
//  HejintaoDemo
//
//  Created by jintao he on 2018/11/13.
//  Copyright © 2018 jintao he. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

