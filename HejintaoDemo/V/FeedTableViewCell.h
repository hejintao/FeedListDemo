//
//  FeedTableViewCell.h
//  HejintaoDemo
//
//  Created by jintao he on 2018/11/14.
//  Copyright © 2018 jintao he. All rights reserved.
//

#import "PRTableViewCell.h"
#import "FeedModel.h"

#define kPostCell_ID    @"kPostCell_ID"

NS_ASSUME_NONNULL_BEGIN

@interface FeedTableViewCell : PRTableViewCell

@property (nonatomic, strong) UIView *imagesView;

+(CGFloat)cellHeightForType:(FeedModel *)pdata;

-(void)setPostData:(FeedModel *)postData;

#pragma mark- public methods
+ (void)createImageViewsForView:(UIView *)imagesView
                     byPostData:(FeedModel *)postData;

+ (CGFloat)oneLineImageHeight:(NSArray *)images;

+ (CGFloat)heightForImages:(NSArray *)images;

@end

NS_ASSUME_NONNULL_END
