//
//  FeedModel.h
//  HejintaoDemo
//
//  Created by jintao he on 2018/11/13.
//  Copyright © 2018 jintao he. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedModel : NSObject

@property (nonatomic, copy) NSString *avatar;

@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *headLine;

@property (nonatomic, strong) NSArray <NSString *>*tags;

@property (nonatomic, copy) NSString *topic;

@property (nonatomic, strong) NSArray <NSString *>*images;

@property (nonatomic, strong) NSNumber *viewCount;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *feed_id;

@end

NS_ASSUME_NONNULL_END
