//
//  TestViewController.m
//  HejintaoDemo
//
//  Created by jintao he on 2018/11/14.
//  Copyright © 2018 jintao he. All rights reserved.
//

#import "TestViewController.h"
#import "FeedModel.h"
#import "PRTableView.h"
#import "FeedTableViewCell.h"

@interface TestViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *dataMAry;
@property (nonatomic, strong) PRTableView *tableView;

@property (nonatomic, strong) NSMutableDictionary *cellHeightDic;

@end

@implementation TestViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    AdjustsScrollViewInsetNever(self, self.tableView);
    self.view.backgroundColor = [UIColor whiteColor];
    _dataMAry = [NSMutableArray array];
    [self getDataFromLocal];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuideBottom);
    }];
    
}

-(void)getDataFromLocal{
    
    if (self.dataMAry.count > 0) {
        [self.dataMAry removeAllObjects];
    }
    
    NSString *soureName = @"testData.json";
    NSData *testData = [self dataWithSourceName:soureName];
    
    NSArray *array = [testData jsonValueDecoded];
    
    for (NSDictionary *resultDic in array) {
        @autoreleasepool {
            FeedModel *model = [FeedModel modelWithDictionary:resultDic];
            [self.dataMAry addObject:model];
        }
    }
    
    [self.tableView reloadData];
    
}

/** 获取资源  */
- (NSData *)dataWithSourceName:(NSString *)name {
    NSString *path = [[NSBundle mainBundle]pathForResource:name ofType:@""];
    if (path == nil) {
        return nil;
    }else {
        return [NSData dataWithContentsOfFile:path];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataMAry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FeedModel *model = self.dataMAry[indexPath.row];
    
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPostCell_ID forIndexPath:indexPath];
    [cell setPostData:model];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self parseHeightForRowAtIndexPath:indexPath];
}

-(CGFloat)parseHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeedModel *data = _dataMAry[indexPath.row];
    
    NSString *cacheKey = [NSString stringWithFormat:@"%@%@",data.feed_id,data.description];
    
    CGFloat cellHeight = [self.cellHeightDic[cacheKey] floatValue];
    
    if (cellHeight == 0) {
        cellHeight = [FeedTableViewCell cellHeightForType:data];
        [self.cellHeightDic setObject:@(cellHeight) forKey:cacheKey];
    }
    return cellHeight;
}

#pragma mark -- lazy


-(PRTableView *)tableView{
    if (!_tableView) {
        _tableView = [[PRTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        TableViewNOXIBRegisterCell(_tableView, FeedTableViewCell, kPostCell_ID)
    }
    return _tableView;
}

/*
 缓存cell高度
 */
-(NSMutableDictionary *)cellHeightDic{
    if (!_cellHeightDic) {
        _cellHeightDic = [[NSMutableDictionary alloc] init];
    }
    return _cellHeightDic;
}


@end
